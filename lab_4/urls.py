from django.urls import path
from .views import add_note, index, note_list

urlpatterns = [
    path('', index, name='index-lab4'),
    path('add-note', add_note, name='add-note'),
    path('note-list', note_list, name='note-list')
]