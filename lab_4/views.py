from django.shortcuts import render
from lab_4.forms import NoteForm
from lab_2.models import Note
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.

def index(request):
    notes = Note.objects.all() # Mengambil seluruh Note yang ada di database
    response = {'notes': notes} #notes: query set (list berisi model) --> bakal dimasukkan ke lab4_index.html
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-4')
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all() # Mengambil seluruh Note yang ada di database
    response = {'notes': notes} #notes: query set (list berisi model) --> bakal dimasukkan ke lab_2.html
    return render(request, 'lab4_note_list.html', response)