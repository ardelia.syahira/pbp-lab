from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from .models import Note

# Create your views here.

def index(request):
    notes = Note.objects.all() # Mengambil seluruh Note yang ada di database
    response = {'notes': notes} #notes: query set (list berisi model) --> bakal dimasukkan ke lab_2.html
    return render(request, 'lab_2.html', response)

def xml(request):
    notes = Note.objects.all()
    data = serializers.serialize('xml', notes)
    return HttpResponse(data, content_type="application/xml")

def json(request):
    notes = Note.objects.all()
    data = serializers.serialize('json', notes)
    return HttpResponse(data, content_type="application/json")
