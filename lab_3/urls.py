from django.urls import path
from .views import add_friend, index

urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('add', add_friend)
]