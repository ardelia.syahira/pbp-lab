from django.shortcuts import render
from datetime import datetime, date
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.http.response import HttpResponseRedirect

def index(request):
    friends = Friend.objects.all() # Mengambil seluruh Friend yang ada di database
    response = {'friends': friends} #friends: query set (list berisi model) --> bakal dimasukkin ke friend.lab1html
    return render(request, 'lab3_index.html', response)

def add_friend(request):
    context ={}
  
    # create object of form
    form = FriendForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-3')
    context['form']= form
    return render(request, "lab3_form.html", context)